//console.log("Hellow World!");

// [SECTION] JavaScript Synchronous vs Asynchronous

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

// Fetch() method returns a promise that resolves to a response object
// promise - is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value
console.log(fetch('https://jsonplaceholder.typicode.com/posts'))


// then() captures the response object and returns another promise which will eventually be resolved or rejected
fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response.status));


// "json()" - from the response object to convert data retrieved into JSON format to be used in our application
// Print the converted JSON value from the "fetch" request
fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then((json) => console.log(json));
// "promise chain" - using multiole .then()

/*fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response.json()));*/

// async and await

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
}

fetchData();

// [SECTION] Creating a post

fetch('https://jsonplaceholder.typicode.com/posts', {
	// Sets the method of the "request object"
	method: 'POST',
	// Sets the header data of the "request object" to be sent to the backend
	headers: {
		'Content-type': 'application/json'
	},
	// JSON.stringify - converts the object data into a stringified JSON
	body: JSON.stringify({
		"userID": 1,
		"title": "Create Post",
		"body": "Create Post File"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Updating a post using PUT method

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	// Sets the method of the "request object"
	method: 'PUT',
	// Sets the header data of the "request object" to be sent to the backend
	headers: {
		'Content-type': 'application/json'
	},
	// JSON.stringify - converts the object data into a stringified JSON
	body: JSON.stringify({
		"userID": 1,
		"title": "Nice",
		"body": "Nice one"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Updating a post using DELETE method

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});