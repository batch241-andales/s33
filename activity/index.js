fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => console.log(json.map(function (obj){
	let x = {
		"title": obj.title
	}
	return Object.values(x).toString()
})));

//GET

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => console.log(json));


//POST
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
		"userID": 1,
		"title": "Create Post",
		"completed": false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PUT
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
		"userID": 1,
		"title": "Update Post",
		"description": "Updating the post",
		"completed": "Pending" ,
		"date completed": "Pending" 
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
		"userID": 1,
		"title": "Update Post",
		"description": "Updating the post",
		"completed": true,
		"date completed": "07/09/21"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts/100', {
	method: 'DELETE'
});